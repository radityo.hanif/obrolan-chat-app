const socket = io('http://localhost:3000')

// Built in event when use successfuly connected with web socket server
socket.on('connect', () => {
    swal({
        icon: 'success',
        title: 'Successfully connected with server',
        text: `You connected with id ${socket.id}`
    })

    // Register username
    swal({
        'content' : 'input',
        'title' : 'Masukkan username kamu :'
    })
    .then(username => {
        console.log(username)
        if(username === '' || username == null) {
            location.reload()
        }
        $('#username').html(username)
        socket.emit('form-register-username', username)
    })
})

const messageForm = document.getElementById('message-form')
const sendButton = document.getElementById('send-button')

const createClientChat = (message) => {
    // check if message is empty string
    if (message === '') return
    
    // display element
    const template =
        `
        <div class="media media-chat media-chat-reverse">
            <div class="media-body">
                <p>${message}</p>
            </div>
        </div>
        `
    $('#chat-content').append(template)

    // reset input message form
    messageForm.value = ''

    // check if is private chat or not
    let isPrivate = false
    let username = false
    if($('#username').attr('data-private-chat') != null) {
        isPrivate = true
        username = $('#username').attr('data-private-chat')
    }

    // send message to server
    socket.emit('send-message', {message, isPrivate, username})
}
const createServerChat = (message, id) => {
    // display element
    const template =
        `
        <div class="media media-chat">
            <div class="media-body">
                <p>${message}</p>
                <p class="meta" style="cursor: pointer;">${id}</p>
            </div>
        </div>
        `
    $('#chat-content').append(template)

    // reset input message form
    messageForm.value = ''

    // distribusi fungsi ketika usename user lain diklik
    $(".meta").click((element) => { 
        swal(`Kamu sedang melakukan chat private dengan ${element.target.outerText}`)
        let currentUsername = $('#username').html()    
        $('#username').html('')
        $('#username').html(currentUsername + ' (private 👉) ' + element.target.outerText)
        // indicate chat mode to private
        $('#username').attr('data-private-chat', element.target.outerText)
    })
}

// Execute a function when the user presses a key on the keyboard
messageForm.addEventListener("keypress", (event) => {
    if (event.key === "Enter") {
        event.preventDefault()
        createClientChat(messageForm.value)
    }
})

// Execute a function when the user presses sendButton element
sendButton.addEventListener('click', () => {
    createClientChat(messageForm.value)
})

// When server receive message from another user
// display those message
socket.on('receive-message', (message, user_id) => {
    createServerChat(message, user_id)
})

// When server send event broadcastMessage
socket.on('broadcastMessage', data => {
    swal({
        icon: data.icon,
        title: data.message
    })
}) 