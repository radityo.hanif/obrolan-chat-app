const PORT = 3000
const sockets = require('socket.io')(PORT, {
    cors: {
        origin: ["http://127.0.0.1:5500"]
    }
})

var username = {}

sockets.on('connection', (socket) => {
    console.log(`User with id ${socket.id} connected to our server`)
    username[socket.id] = ''
    
    socket.on('form-register-username', usernameInput => {
        username[socket.id] = usernameInput
        console.log('daftar username', Object.values(username))
    })
    
    socket.on('send-message', data => {
        // data =  {
        //     isPrivate : false,
        //     message : 'hello there'
        // }
        // username = {
        //     '1231asda23' : 'hanif',
        //     '5452baca21' : 'rendi'
        // }
        if(data['isPrivate']) {
            console.log(data['message'], 'pesan private dari', username[socket.id], 'untuk', data['username'])
            Object.keys(username).map((key) => {
                if(username[key] == data['username']) {
                    console.log(key, username[key], data['username'])
                    // to individual socketid (private message)
                    sockets.to(key).emit('receive-message', data['message'], username[socket.id])
                }
            })
        }
        else{
            console.log(data['message'],'pesan publik dari', username[socket.id])
            socket.broadcast.emit('receive-message', data['message'], username[socket.id])
        }
    })
    
    socket.on('disconnect', () => {
        console.log(`${socket.id} disconnect from our server`)
        sockets.emit('broadcastMessage', {
            icon: 'warning',
            message: `${username[socket.id]} Keluar dari room chat`
        })
    })
})