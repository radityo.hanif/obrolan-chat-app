## Obrolan
**Simple chat room web based app**
implemented using node js and socket.io https://socket.io/

## Available Features
- Real time chat
- Public chat
- Private chat
- Another user disconnection alert

## Preview
![App Screenshot](preview.jpg)